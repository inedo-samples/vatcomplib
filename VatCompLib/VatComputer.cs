﻿namespace VatCompLib;

public static class VatComputer
{
    /// <summary>
    /// Computes the VAT due based on a given Sale amount and VAT Rat
    /// </summary>
    public static decimal Compute(decimal sale, decimal rate)
    {
        return sale * rate;
    }
}